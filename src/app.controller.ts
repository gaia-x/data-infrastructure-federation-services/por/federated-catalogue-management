import { Controller, Get, Post } from '@nestjs/common';
import { Public } from './auth/auth.guard';
@Controller()
export class AppController {
  constructor() {}

  @Public()
  @Get("isAlive")
  ping() {
    return {
      status: 'running',
      server_time: new Date().toISOString(),
    };
  }
}
