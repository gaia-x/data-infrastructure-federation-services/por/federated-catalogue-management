import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServerCache } from './util.cache';
import { HttpResolver } from './util.http-resolver';

@Module({
  imports: [HttpModule, ConfigModule],
  providers: [HttpResolver, ServerCache],
  exports: [HttpResolver, ServerCache],
})
export class UtilModule {}
