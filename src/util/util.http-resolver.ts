import { HttpService } from '@nestjs/axios';
import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosRequestConfig } from 'axios';
import { lastValueFrom, map } from 'rxjs';

@Injectable()
export class HttpResolver {
  access_token: string;
  access_token_exp: number;
  refresh_token: string;
  refresh_token_exp: number;
  id_token: string;
  offsetInSeconds: number = 10;
  password_credential_params = new URLSearchParams();
  refresh_token_params = new URLSearchParams();
  defaultConfig: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
  };

  private readonly logger = new Logger(HttpResolver.name);

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {
    this.password_credential_params.append('grant_type', 'password');
    this.password_credential_params.append(
      'username',
      this.configService.get('HTTPSERVICE_USERNAME'),
    );
    this.password_credential_params.append(
      'password',
      this.configService.get('HTTPSERVICE_PASSWORD'),
    );
    this.password_credential_params.append(
      'client_id',
      this.configService.get('HTTPSERVICE_CLIENTID'),
    );
    this.password_credential_params.append(
      'client_secret',
      this.configService.get('HTTPSERVICE_CLIENT_SECRET'),
    );

    this.refresh_token_params.append('grant_type', 'refresh_token');
    this.refresh_token_params.append(
      'client_id',
      this.configService.get('HTTPSERVICE_CLIENTID'),
    );
    this.refresh_token_params.append(
      'client_secret',
      this.configService.get('HTTPSERVICE_CLIENT_SECRET'),
    );
  }

  async GetAuthorized(
    url: string,
    config: AxiosRequestConfig = this.defaultConfig,
  ) {
    const authConfig: AxiosRequestConfig = {
      headers: { ...config.headers, Authorization: await this.GetToken() },
    };
    return this.Get(url, authConfig);
  }

  async DeleteAuthorized(
    url: string,
    config: AxiosRequestConfig = this.defaultConfig,
  ) {
    const authConfig: AxiosRequestConfig = {
      headers: { ...config.headers, Authorization: await this.GetToken() },
    };
    return this.Delete(url, authConfig);
  }

  async PostAuthorized(
    url: string,
    data: any = {},
    config: AxiosRequestConfig = this.defaultConfig,
  ) {
    const authConfig: AxiosRequestConfig = {
      headers: { ...config.headers, Authorization: await this.GetToken() },
    };
    return this.Post(url, data, authConfig);
  }

  async Get(url: string, config: AxiosRequestConfig = this.defaultConfig) {
    return await lastValueFrom(
      this.httpService.get(url, config).pipe(map((resp) => resp.data)),
    ).catch((err) => {
      this.logger.error(err);
      throw new HttpException(
        err.response?.data ?? {},
        err.response?.status ?? 500,
      );
    });
  }

  async Post(
    url: string,
    data?: any,
    config: AxiosRequestConfig = this.defaultConfig,
  ) {
    return await lastValueFrom(
      this.httpService.post(url, data, config).pipe(map((resp) => resp.data)),
    ).catch((err) => {
      this.logger.error(err);
      throw new HttpException(
        err.response?.data ?? {},
        err.response?.status ?? 500,
      );
    });
  }

  async Delete(url: string, config: AxiosRequestConfig = this.defaultConfig) {
    return await lastValueFrom(this.httpService.delete(url, config)).catch(
      (err) => {
        this.logger.error(err);
        throw new HttpException(
          err.response?.data ?? {},
          err.response?.status ?? 500,
        );
      },
    );
  }

  private async GetToken(): Promise<string> {
    const currentDateInMs = new Date().getTime();
    if (this.access_token == undefined) {
      await this.FetchNewTokens();
    } else if (
      this.access_token_exp < currentDateInMs &&
      this.refresh_token_exp < currentDateInMs
    ) {
      await this.FetchNewTokens();
    } else if (
      this.refresh_token_exp > currentDateInMs &&
      this.access_token_exp < currentDateInMs
    ) {
      this.password_credential_params.append(
        'refresh_token',
        this.refresh_token,
      );
      await this.FetchNewTokens(true);
    }

    return `Bearer ${this.access_token}`;
  }

  private async FetchNewTokens(refresh = false) {
    var config: AxiosRequestConfig = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    };

    var response: any;
    if (refresh == false) {
      response = await this.Post(
        this.configService.get('HTTPSERVICE_TOKENURL'),
        this.password_credential_params,
        config,
      );
    } else {
      response = await this.Post(
        this.configService.get('HTTPSERVICE_TOKENURL'),
        this.refresh_token_params,
        config,
      );
    }
    if (response) {
      const {
        access_token,
        expires_in,
        refresh_token,
        refresh_expires_in,
        id_token,
      } = response;
      this.access_token = access_token;
      this.access_token_exp = expires_in - this.offsetInSeconds * 1000;
      this.refresh_token = refresh_token;
      this.refresh_token_exp = refresh_expires_in - this.offsetInSeconds * 1000;
      this.id_token = id_token;
    } else {
      throw new InternalServerErrorException('wrong server configruration');
    }
  }
}
