import { Injectable } from '@nestjs/common';
import { ServerCache } from 'src/util/util.cache';

@Injectable()
export class OfferingService {
  constructor(private readonly cache: ServerCache) {}

  getOfferings() {
    return this.cache.get('offerings');
  }
  setOfferings(data: unknown) {
    return this.cache.set('offerings', data);
  }

  registerOfferingForUpload(data: any) {
    var outstandingOffers = ((this.cache.get('registerdCatalogue') as [any]) ??
      []) as [any];
    outstandingOffers.push(data);
    return this.cache.set('registerdCatalogue', outstandingOffers);
  }

  registerAttesttionForQueue(data: any) {
    var outstandingAttestations = ((this.cache.get('registerdAttestation') as [
      any,
    ]) ?? []) as [any];
    outstandingAttestations.push(data);
    return this.cache.set('registerdAttestation', outstandingAttestations);
  }
  getOpenAttestations() {
    return this.cache.get('registerdAttestation');
  }

  setOpenAttestations(data: any) {
    return this.cache.set('registerdAttestation', data);
  }
}
