import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateOfferingDto {
  @ApiProperty()
  @IsString()
  did: string;
}
