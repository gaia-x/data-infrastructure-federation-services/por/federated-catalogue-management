import { Module } from '@nestjs/common';
import { OfferingService } from './offering.service';
import { OfferingController } from './offering.controller';
import { UtilModule } from 'src/util/util.module';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [UtilModule, HttpModule],
  controllers: [OfferingController],
  providers: [OfferingService],
})
export class OfferingModule {}
