import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UtilModule } from 'src/util/util.module';

import { CatalogueController } from './catalogue.controller';
import { CatalogueService } from './catalogue.service';

@Module({
  imports: [UtilModule, ConfigModule],
  controllers: [CatalogueController],
  providers: [CatalogueService],
})
export class CatalogueModule {}
