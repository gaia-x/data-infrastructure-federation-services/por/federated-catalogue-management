import { Controller, Get, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { CatalogueService } from './catalogue.service';
import { LimitOffsetDto } from './dto/limit-offset.dto';
import { SearchStringDto } from './dto/search-string.dto';

@ApiTags('catalogue')
@ApiBearerAuth()
@Controller()
export class CatalogueController {
  constructor(private readonly catalogueService: CatalogueService) {}

  // @Post('/upload')
  // create(@Body() createCatalogueDto: CreateCatalogueDto) {
  //   return this.catalogueService.create(createCatalogueDto);
  // }

  // @Post('/query')
  // query(
  //   @Query() queryParams: LimitOffsetDto,
  //   @Body() queryCatalogDto: QueryCatalogueDto,
  // ) {
  //   return this.catalogueService.query(queryParams, queryCatalogDto);
  // }

  @Get('query')
  getQuery(
    @Query() searchString: SearchStringDto,
    @Query() queryParams: LimitOffsetDto,
  ) {
    return this.catalogueService.searchInCache(
      searchString.search,
      parseInt(queryParams.limit),
      parseInt(queryParams.offset),
      searchString.type,
    );
  }

  // @Delete('/delete/:hash')
  // remove(@Param('hash') hash: string) {
  //   return this.remove(hash);
  // }
}
