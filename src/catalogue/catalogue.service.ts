import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Interval } from '@nestjs/schedule';
import { ServerCache } from 'src/util/util.cache';
import { HttpResolver } from 'src/util/util.http-resolver';
import { CreateCatalogueDto } from './dto/create-catalogue.dto';
import { LimitOffsetDto } from './dto/limit-offset.dto';
import { QueryCatalogueDto } from './dto/query-catalogue.dto';

@Injectable()
export class CatalogueService {
  private readonly logger = new Logger(CatalogueService.name);

  constructor(
    private readonly cache: ServerCache,
    private readonly httpResolver: HttpResolver,
    private readonly configService: ConfigService,
  ) {}

  // create(createCatalogueDto: CreateCatalogueDto) {
  //   return this.httpResolver.PostAuthorized(
  //     `${this.configService.get('CATALOG_ENDPOINT')}/self-descriptions`,
  //     createCatalogueDto,
  //   );
  // }

  // query(queryParams: LimitOffsetDto, queryCatalogDto: QueryCatalogueDto) {
  //   const { limit, offset, ...rest } = queryParams;
  //   const searchParams = rest ? `?${new URLSearchParams(rest)}` : '';
  //   return this.httpResolver.Post(
  //     `${this.configService.get('CATALOG_ENDPOINT')}/query${searchParams}`,
  //     {
  //       ...queryCatalogDto,
  //       parameters: {
  //         ...queryCatalogDto.parameters,
  //         limit: parseInt(limit),
  //         offset: parseInt(offset),
  //       },
  //     },
  //   );
  // }

  // remove(hash: string) {
  //   return this.httpResolver.DeleteAuthorized(
  //     `${this.configService.get('CATALOG_ENDPOINT')}/self-descriptions/${hash}`,
  //   );
  // }

  searchInCache(
    searchString: string,
    limit: number,
    offset: number,
    type?: string,
  ) {
    var result = [];
    var offerings = this.cache.get('offerings') ?? {};

    var array = Object.values(offerings);
    if (offset >= array.length) {
      return { totalCount: result.length, items: result };
    }

    if (limit + offset > array.length) limit = array.length;

    for (var i = offset; i < limit; i++) {
      const did = array[i];
      if (searchString) {
        var stringfied = JSON.stringify(did).toLowerCase();
        if (stringfied.includes(searchString.toLowerCase()))
          result.push(did['gx-catalog-description'].full);
      } else result.push(did['gx-catalog-description'].full);
    }
    if (type) {
      result = result.filter(
        (item) =>
          item['credentialSubject']?.['description']?.['@type']
            ?.toLowerCase()
            .includes(type.toLowerCase()) ||
          item['credentialSubject']?.['@type']
            ?.toLowerCase()
            .includes(type.toLowerCase()),
      );
    }
    return { totalCount: result.length, items: result };
  }

  // @Interval(3000)
  // async updateCatalogueByOfferings() {
  //   let offerings = this.cache.get('registerdCatalogue') as [] | undefined;
  //   if (!offerings) {
  //     return;
  //   }

  //   while (offerings.length > 0) {
  //     let offer = offerings.pop();
  //     this.cache.set('registerdCatalogue', offerings);
  //     try {
  //       await this.create(offer);
  //     } catch (e) {
  //       this.logger.debug(e);
  //       this.logger.verbose('unable to upload');
  //     }
  //   }
  // }
}
