import { ApiProperty } from '@nestjs/swagger';
import { IsNumberString } from 'class-validator';

export class LimitOffsetDto {
  @ApiProperty({ default: 10 })
  @IsNumberString()
  limit: string;
  @ApiProperty({ default: 0 })
  @IsNumberString()
  offset: string;
}
