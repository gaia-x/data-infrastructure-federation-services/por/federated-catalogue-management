import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsOptional, IsString } from 'class-validator';

export class QueryCatalogueDto {
  @ApiProperty()
  @IsString()
  statement: string;
  @ApiPropertyOptional()
  @IsOptional()
  @Transform(({ value }) => new Map(Object.entries(value)))
  parameters?: Map<string, number | string>;
}
