import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDefined, IsString } from 'class-validator';

export class CreateCatalogueDto {
  @ApiProperty()
  @IsString()
  '@id': string;
  @ApiProperty()
  @IsDefined()
  proof: any;
  @ApiProperty()
  @IsArray()
  @IsString({ each: true })
  type: ['VerifiablePresentation'];
  @ApiProperty()
  @IsArray()
  @IsString({ each: true })
  '@context': ['https://www.w3.org/2018/credentials/v1'];
  @ApiProperty()
  @IsDefined()
  verifiableCredential: any;
}
