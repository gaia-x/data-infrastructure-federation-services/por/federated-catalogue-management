import { ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { OIDCGuard } from './auth/auth.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.enableCors();
  //app.useGlobalGuards(new OIDCGuard(app.get(Reflector)));

  const config = new DocumentBuilder()
    .setTitle('Api-Docs')
    .setVersion('1.0')
    // .addOAuth2({
    //   flows: {
    //     password: {
    //       tokenUrl: process.env.OAUTH2_CLIENT_TOKEN_URL,
    //       scopes: { openid: 'openid' },
    //       authorizationUrl: process.env.OAUTH2_CLIENT_TOKEN_URL,
    //     },
    //   },
    //   type: 'oauth2',
    // })
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api-docs', app, document);

  await app.listen(3000);
}
bootstrap();
